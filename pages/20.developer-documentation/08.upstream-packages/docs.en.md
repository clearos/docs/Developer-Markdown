---
title: 'Upstream Packages'
taxonomy:
    category:
        - docs
visible: true
---

There may be times when you find yourself needing to possibly compile and package code to be deployed through the ClearOS RPM repositories.  Examples include:
* having original source code for a project (eg. [ClearSync](https://gitlab.com/clearos/clearfoundation/clearsync))
* there is a need to recompile existing code to better suit the needs of ClearOS (eg. [OpenLDAP](https://gitlab.com/clearos/upstream/centos/openldap/tree/c7))
* having tar files for a project but no RPM package in mainstream repositories - ClearOS, CentOS, EPEL (eg. [NextCloud](https://gitlab.com/clearos/contribs/nextcloud))

In these cases, the ClearOS Koji build servers can be provided specifically formatted files in order to checkout code from software git repositories on the Internet and build the required packages.

It's probably easier to follow this workflow with an example, so we are going to use NextCloud as a template to explain the process.

### Setting Up

The first thing you should do is create a folder for upstream ClearOS packages in your developmemnt environment.  A logical (but not required) location would be 'upstream' in your home directory.

    cd ~
    mkdir upstream
    cd upstream

### Check Out Source Code

Create a git repository for your source code, then checkout your empty project.

!!! TODO:  Source code must reside in Gitlab

   git clone git@gitlab.com:clearos/contribs/nextcloud.git nextcloud

### Clone ClearOS Common Build Tools

In your upstream folder, checkout/clone the "Common Clear Build Tools".

    git clone https://gitlab.com/clearos/common common
    
Create a symlink from your cloned project to the Makefile found in the common build tools:

    cd ~/upstream/nextcloud
    ln -s ../common/Makefile .

### Add and Commit Your Files

At a minimum, you will require a spec file.  If you've never built an RPM, it's beyond the scope of this documentation, however, we've provided some helpful instruction [here](/upstream-packages/rpm-basics).

For our example, NextCloud provides tar archives of their software [here](https://nextcloud.com/install/#instructions-server).

Let's have a look at the some of the files in [the parent folder](https://download.nextcloud.com/server/releases/) for a given version (let's say, 13.04).

    nextcloud-13.0.4.tar.bz2         2018-06-11 08:30	43M	 
    nextcloud-13.0.4.tar.bz2.asc     2018-06-11 08:30	819	 
    nextcloud-13.0.4.tar.bz2.md5     2018-06-11 08:30	59	 
    nextcloud-13.0.4.tar.bz2.sha256  2018-06-11 08:30	91	 
    nextcloud-13.0.4.tar.bz2.sha512  2018-06-11 08:30	155	 
    nextcloud-13.0.4.zip             2018-06-11 08:30	59M	 
    nextcloud-13.0.4.zip.asc         2018-06-11 08:30	819	 
    nextcloud-13.0.4.zip.md5         2018-06-11 08:30	55	 
    nextcloud-13.0.4.zip.sha256      2018-06-11 08:30	87	 
    nextcloud-13.0.4.zip.sha512      2018-06-11 08:30	151	 

Perfect.  Of these files, we're going to grab the compressed tar file (nextcloud-13.0.4.tar.bz2).  Not that it would be difficult to do ourselves, but we're also going to need the SHA256 checksum so that the build system can validate the download.  See the Download Sources section below.
### Spec File

Again, the content of the spec file is beyond this document, however, there are no shortage of examples on which to form the basis. In this particular, we merged the original OwnCloud spec from a ClearOS contrib with that of an RPM for NextCloud from another [RPM-based FOSS project](https://github.com/NethServer/nethserver-nextcloud/blob/master/nethserver-nextcloud.spec).

### Download Sources

Create a file called <code>download.sources</code>.  It will map out for the build syste the location of the source code repositories required to build the package.

For our NextCloud example, we'll just need the URL for the compressed tar file:

    https://download.nextcloud.com/server/releases/nextcloud-13.0.4.tar.bz2

We don't need the file (nextcloud-13.0.4.tar.bz2.sha256) - just the contents:

    curl https://download.nextcloud.com/server/releases/nextcloud-13.0.4.tar.bz2.sha256
    
Which will output:

    18d514145fcddc86f48d0a5fa4a0d4b07617135a1b23107137a6ea3ed519bd54  nextcloud-13.0.4.tar.bz2
    
In the sources.download file, the syntax is the following variables separated by a single space:
* SHA256 checksum
* filename
* URL

! It is very important to have a newline character at the end of the file or else the build scripts may not pick up on your source files.  Editing on platforms other than Linux or Mac may not include this special character.  To view an example of correcting this error, see [this commit](https://github.com/WikiSuite/syncthing/commit/1c99b016f92482f07362adc4dbdc74bc2b0c1afa#diff-477bb8fbb990fd79a0665b55530d0997).

For our example, we'll add just a single line consisting of:

    18d514145fcddc86f48d0a5fa4a0d4b07617135a1b23107137a6ea3ed519bd54 nextcloud-13.0.4.tar.bz2 https://download.nextcloud.com/server/releases/nextcloud-13.0.4.tar.bz2

Save the file and exit.

## Building Locally

In order to emulate what the build system will use to build the package, use the Makefile you linked earlier from the ClearOS tools.
   
    cd ~/upstream/nextcloud
    make local
   
If all goes well, you'll the required source file(s) will download and the package will be built locally, ending up in <code>~/rpmbuild/RPMS</code> in either the noarch or x86_64 folder.
