---
title: 'Webconfig as a Reverse Proxy'
media_order: syncthing_user_profile.png
taxonomy:
    category:
        - docs
visible: true
---

Webconfig uses a pre-configured, sandboxed Apache engine on alternate port 81 in order to deliver the Webconfig application that can be used to perform all system management, as required.

As more and more Webconfig apps enter the Marketplace, a number of them provide their own web-based application platforms using other stacks like Python, NodeJS, NGINX to name a few.

To reduce the number of end-points users have to remember and increase security by exposing fewer applications directly to the outside world, Webconfig can utilize Apache's reverse proxy capabilties.

! The examples below need more testing and feedback.

## Simple Proxying

An examle of a simple, 1 to 1 route proxy request is to protect and access PHPMyAdmin using Webconfig's reverse proxy.

A sample configlet that drops into /usr/clearos/sandbox/etc/httpd/conf.d/ demonstrates this use case.

    ProxyRequests Off

    ProxyPass /phpMyAdmin http://127.0.0.1/phpMyAdmin 
    ProxyPassReverse /phpMyAdmin http://127.0.0.1/phpMyAdmin

## Proxying with 2FA

To use Webconfig's two-factor authentication app priort to allowing access to a web application behind the reverse proxy, Apache's rewrite engine must be used in placed of the built-in ProxyPass sub-module.  Below is an example file to allow the user 'admin' who has authenticated via webconfig (using 2FA).

    ProxyRequests off

    <Location /phpMyAdmin/>
        AuthName "phpMyAdmin"
        AuthType Basic
        AuthBasicProvider external
        AuthExternal pwauth
        Require unix-group allusers
        IncludeOptional conf.d/two_factor_auth.*
        RewriteEngine on
        RewriteCond %{REMOTE_USER} "admin"
        RewriteRule "/phpMyAdmin/(.*)" "http://127.0.0.1/$1 [P]
    </Location>

## Multi-User Proxying

A more complex use of the reverse proxy is required if the redirection is different depending on the user.  An examle use-case is the Syncthing app, which binds user's Syncthing management portal to unique ports in order to identify a user.

### Apache Configlet

We start off with a skeleton configlet file for what we want to achieve...the important aspects of the file will be outlined below:

    ProxyRequests off

    <Location /syncthing/>
        AuthName "Syncthing User"
        AuthType Basic
        AuthBasicProvider external
        AuthExternal pwauth
        Require unix-group syncthing_plugin
        IncludeOptional conf.d/two_factor_auth.*
        RewriteEngine off
    </Location>
    
### ProxyRequests
For security purposes, we disable proxy requests in this configlet until authentication has been established.

### Location
This is the unique (or namespaced) route that will trigger this proxy request.

### AuthName
What is presented to the user during authentication.

### AuthType
Authentication type - Basic Auth is used in the example here, but other methods such as cookies or custom headers could be used.

### AuthBasicProvider
Invokes the Basic Auth module and indicates we are going to use a custom method to authenticate our users with.

### AuthExternal
The application we use to authentication users...pwauth is a handy utility to plug into PAM.

### IncludeOptional
Any additional configlets to include, if they exist.  In this example, we plugin in the two factor authentication requirement if 2FA app is installed.

### RewriteEngine
By default, this configlet will not perform proxying requests.  It will be enabled programatically via Webconfig.

