---
title: 'App Development'
visible: true
taxonomy:
    category: docs
---

## What Constitutes an App?
In order for an app to qualify to be listed in the ClearOS Marketplace, it must meet minimum requirements in
each of the following criteria listed below:
1. Function
2. Packaging
3. Maintenance
4. Support

### Function
By function, we mean the app must _do something_ to enhance the functionality of the core platform. Do something? That's a pretty broad stroke...perhaps the following example will help with this definition.

If your company provides security services for Linux gateways, including penetrations tests and consulting services to ensure a level of security, you may
be very interested in marketing your services the ClearOS install base. One way of creating an app that would be declined from entry to the Marketplace would be to create a simple app that provides your company's logo, contact info and website link with a description of the services you provide. Sorry – wrong approach.

Consider, however, if your company took the time to create an app that, either free of charge or for a fee, performed an automated pen-test and provided a report back to the user on a daily or weekly basis. This app would certainly meet the minimum 'function' element by providing the user with something of value. As a by-product, your app would help drive awareness of your organization's expertise and services and market to the ClearOS install base.

### Packaging
Being derived from RHEL, ClearOS obviously uses the RPM package manager. A ClearOS app consists of at least two packages:
1. app-**_basename_**
2. app-**_basename_**-core

Where **_basename_** depends on the naming convention of the app (eg. app-mail-smtp would be expected to provide the Marketplace MVC components to manage an SMTP server – in this case, Postfix). You don't have to become an RPM packaging expert to become a ClearOS developer...the heavy lifting for the creation of the spec file is done by automation scripts available as developer tools on ClearOS.

Depending on any additional software that may be dependent on your app, additional packages may be required. In many cases, these are available through one or more ClearOS repositories that are maintained by the ClearFoundation and fed by upstream sources.

If you do require code that has not been packaged in RPM file format, you will need to do so or engage ClearCenter's consulting services to take on this task for you. We do not accept apps that cannot do version control via RPM – scripts using wget to pull down tarballs from 3rd party websites...we're looking at you.

### Maintenance
ClearCenter strives to provide a high-quality, secure and stable Marketplace. Developers and ISV's creating apps that are introduced to the Marketplace need to maintain a base minimum to ensure software bestpractices are met. Apps (both free or paid) need to meet the following minimum requirements or they will be pulled:

1. If a flaw is reported that compromises the security of the system, this flaw will be fixed and backported.
2. If a bug is reported that affects the core function of the app, best efforts will be made to resolve the issue in a timely manner (within 30 days).
3. If a sole software dependency exists to another library or project, that project will be monitored by the app developer for updates fixing security exploits or significant bugs. The app developer will be responsible for providing the upgrade to the ClearOS community.

### Support
The support requirement varies depending on whether your app falls in the category of free, paid or
subscription.

**Free** – There is no onus or responsibility for a developer providing a free app into the Marketplace to provide support to anyone who considers installing the app. Good karma if you share/dedicated some time to the user forums if questions arise.

**Paid** – If an app enters the Marketplace as a paid submission, the developer or organization accepts a degree of responsibility to provide some assistance on open user forms (via ClearFoundation.com or elsewhere). As a paid app, the developer is essentially agreeing to become 'champion' of a specific feature which the app provides – sharing their knowledge and helping users who experience difficulties.

There is no Service Level Agreement or guarantee entitling users of a paid app to support and this will be clearly defined in the apps support policy.
Developers are free to create their own SLA (and charge more for the additional time/investment). For example, two apps sharing similar complexity might be priced rather differently...one at $10 and another at $150. The developer of the $150 app may have priced the app in such a way as to be able to include free technical support for 60 days, ensuring users of a good first impression and experience.

**Subscription** - Subscription apps with recurring fee structure are generally reserved for two scenarios:

1. A recurring service – for example, providing IDS signature updates on a weekly basis for as long as the subscription is renewed
2. Support – an app that continues with a continuous support policy to be able to seek commercial, professional support from the apps developer.

! Depending on the app complexity, ClearCenter reserves the right to decide whether an app in the Marketplace (at the time of submission) falls under the coverage of the commercial (and thus supported) ClearOS Professional subscriptions.

## Marketplace Revenue Model
### Revenue Share
ClearCenter is committed to creating an ecosystem where app developers can generate sustainable revenues from their submissions. To this end, the standard revenue share is split 70:30 – Developer to ClearCenter.

There are cases where this share percentage is negotiated, such as:

* joint venture where shared development resources are used
* ISV's reselling software via the ClearOS Marketplace

Please contact ClearCenter before starting development to discuss the revenue share opportunity for an app you or your organization are thinking of creating for submission to the Marketplace.

### Payment Schedule
Unless otherwise negotiated, revenues will be paid out by ClearCenter to the developer/organization on a quarterly basis if total revenues (of all apps in submission by developer) do not exceed $US 1,000, and on a monthly basis if revenue exceed $US 1,000.

### Payment Transfers
Unless otherwise agreed upon, payment transfers from ClearCenter to a developer will be done via PayPal.

### Currency
ClearCenter's Marketplace currently accepts payments in USD only. All submissions will be priced in US dollars.

### Minimum Price
The minimum app price in the ClearOS Marketplace is US $25.

### Product Bundles
ClearCenter may want to invoke 'product bundles' using an app created by you. In line with 'bundle theory', the cost of the bundle may be less than the sum of the individual apps.

Bundles can help with your apps adoption in creating higher value for customers and adding ease of use. Consider the mail stack on the ClearOS platform. There are over a dozen apps that would benefit from being bundled into 3 or 4 offerings. For example:

* OSE Bundle – mail-smtp, mail-imap, mail-greylisting, mail-antimalware, mail-antispam
* Home Bundle – mail-smtp, mail-greylisting, mail-antimalware, mail-antispam, zarafa-community
* SMB Bundle - mail-smtp, mail-greylisting, mail-antimalware, mail-antispam, zarafa-small-business, kasperskymail

ClearCenter may request inclusion of your app into a bundle. If your app is paid, a negotiated revenue share less than the selling price of the individual app will need your agreement prior to inclusion.

### Marketplace Exclusivity
A developer can choose whether the app is released to all ClearOS platforms or is exclusive to one or more editions (eg. ClearOS Professional Edition). Under certain circumstances, ClearCenter may require limiting the availability of an app to specific platforms.

### Revenue and/or Usage Reports
A developer or organization can track installation/purchase history via the ClearCenter portal. Once your account is setup as a Developer, an additional heading (“Developer”) will be visible. Select “Unit/Sales Tracking” from the sub-menu. Apps written by the developer can be selected along with a toggle to display by unit installs or revenues. Obviously, if an app is free, tracking of these apps can only be done by unit installs.

Use the sales tracking feature to invoice ClearCenter on either a monthly or quarterly basis.
