---
title: 'Dashboard Widgets'
visible: true
taxonomy:
    category: docs
---

Dashboard widgets are provided by the app when you install them (and removed if you un-install the app).  Essentially, they are just the same Controller/View (in the MVC model) that powers all other Webconfig pages, with the exception that they follow a naming convention and are defined in an apps metadata file (/deploy/info.php).
    
## Writing the Controller/View

In order for a dashboard widget to hook into the Webconfig framework and the ClearOS Dashboard page, you need to create a controller with the format:

    <basename>_dashboard.php.

The Controller can have any number of routes/functions defined, each displaying different information.  Each function will be defined as a separate dashboard widget that an administrator can configure to be displayed or not.  For example, if you had an app that reported on system diagostics, a deveper would have the choice of calling a single dashbaord widget that displayed multiple pieces of information (eg. CPU load, temperatures, hard disk usage etc.) under one single function with the view displaying this information in a pre-determined layout.  Alternatively, a developer could separate these functions into 3 separate dashboard widgets under distinct functions, allowing the admin to control which to display and where in the dashboard layout.

## Making the Dashboard Widget Available

Once your controller and view(s) have been completed, you hook into the Webconfig Dashboard widget.  To do this, simply edit the <code>/deploy/info.php</code> app definition/metadata file and add:

    /////////////////////////////////////////////////////////////////////////////
    // Dashboard Widgets
    /////////////////////////////////////////////////////////////////////////////

    $app['dashboard_widgets'] = array(
        $app['category'] => array(
            '<basename>/<basename>_dashboard/widget_1' => array(
                'title' => lang('widget_1'),
                'restricted' => FALSE,
            ),
            '<basename>/<basename>_dashboard/widget_2' => array(
                'title' => lang('widget_2'),
                'restricted' => TRUE,
            ),
        )
    );

#### Parameters

##### Title
The title displayed on the widget selector.

##### Restricted
The restricted parameter, if set to boolean TRUE, will only be made available on the 'root' account.

## Widgets Using JavaScript

The dashboard will pull in required Javascript files as required.  See section TODO.