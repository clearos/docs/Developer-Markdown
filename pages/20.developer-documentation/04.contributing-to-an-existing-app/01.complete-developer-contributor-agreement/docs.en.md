---
title: 'Complete Developer Contributor Agreement'
taxonomy:
    category:
        - docs
visible: true
---

## Contributor Agreement

In order to protect ClearFoundation and its projects, we ask that developers who contribute intellectual property (including patches and source code) agree to a contributor agreement. The [Free Software Foundation (FSF)](http://www.gnu.org/licenses/why-assign.html) has a concise explanation on why this is standard practice for open source projects.

! This step is only required if you plan on developing/maintaining ClearFoundation apps. If you are developing your own app, those intellectual property rights belong to you!

If you have any questions or concerns, please do not hesitate to contact us - developer@clearfoundation.com.

The online contributor agreement can be completed [here](https://www.clearos.com/clearfoundation/code/community-volunteer-form).