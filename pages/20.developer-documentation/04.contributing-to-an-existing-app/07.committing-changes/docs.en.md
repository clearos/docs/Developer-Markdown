---
title: 'Committing Changes and Building a Package in Koji'
media_order: 'gitlab_users.png,users_sub_check.png'
taxonomy:
    category:
        - docs
visible: true
---

## Local Commit
Once you're happy with your changes, it's time to commit them to the master branch.

    git commit -a
    
! If you've added any new files, you will see them listed here as a warning.

If do have files to add, you can add them:

    git add <path to new file>

Committing with the _-a_ flag will commit all your changes to your local master branch.

!!! For bigger changes, you may want to create a new branch and commit to it rather than master.

## Pushing Commit to Remote Repository (Git)

Once you are ready to commit your changes for other developers to see and potentially push to the Koji build system, you can push your changes to the remote master with:

    git push

## Triggering a Build

If you have permission, you can merge the master code to the reserved branch named **_clear7_**.  This branch is special in that it has a webhook that triggers a Koji build of the new source code.

To merge code from the master branch to the clear7 branch, simply run:

    git checkout clear7
    git merge master
    git push

If everything has gone right, you should see a new task at [http://koji.clearos.com/koji/tasks](http://koji.clearos.com/koji/tasks).