---
title: 'Contributing to an Existing App'
media_order: users_kopano.png
taxonomy:
    category:
        - docs
visible: true
---

## Contributing to an Existing App
There are many reasons why you might want to contribute to an existing app:
* Add to or improve a translation
* Add to or improve documentation\[1\]
* Fix a usability issue
* Fix a bug
* Add a feature

!!! \[1\] Documentation for an app will be moving to the git repositories in late 2018/early 2019.

The sections that follow will go through the entire workflow of contributing to a ClearOS app.

!!!! If you're even thinking about contributing to an existing app...THANK YOU.  If you make a contribution to help improve ClearOS for everyone, well then, it's time to hit the pub!

<a id="example"></a>
## Example 

As is often the case, it's sometimes easiest to follow a real-world example.  The remaining sections of this tutorial on contributing to an existing app will follow exactly that.

ClearOS has a subscription engine built into the framework.  It allows the developer to build apps around CPU core limits, users counts etc.  One example is the licensing of the [Kopano Basic](https://www.kopano.com) - a ClearCenter supported app that integrates Kopano into the ClearOS LDAP/Account directory and provides the Kopano Communication and Collaboration Suite.  A usability issue has been reported in support/forums that on occasion, a user who purchases additional user CALs not from the Marketplace but via an interaction with ClearCenter sales.  In this case, a user sometimes has to wait up to 30 minutes (the cache timeout) for the additional CAL's to appear.  

This is what a user would see if they had used all of their existing CALs, purchased more, but were waiting for the cache to expire:

![Kopano Subscriptions](users_kopano.png)

We could tell them to drop to the command line and run:

    clearcenter-subscriptions -n

To force a check of valid subscriptions and refresh the cached content.  Many users, however, are not familiar with CLI.  Wouldn't it be nice to add a "Refresh Subsciptions" button that triggered the script above.  And so, with this in mind, follow along with the workflow while we make this change.