---
title: 'Response Codes'
visible: true
---

## HTTP Reponse Codes
The ClearOS API uses conventional HTTP response codes for error handling. 

| Code | Response | Description |
| ---- | -------- | ----------- |
| 200 | OK | Request was successful |
| 400 | Bad Request | The client request was invalid |
| 401 | Unauthorized | The API key or credentials were invalid |
| 403 | Forbidden | The API key or credentials were valid, but access was not permitted |
| 404 | Not Found | The resource was not found |
| 405 | Method Not Allowed | The method is not supported by the API |
| 409 | Conflict | Resource already exists |
| 422 | Validation Error | Parameters sent in the request were invalid |
| 429 | Too Many Requests | Too many requests were sent to the server |
| 500 | Server Error | Server-side error (rare) |

## Extended Response Codes
In addition to the HTTP response codes, a status code and message are also passed in the JSON payload.   Here’s an example:

```json
{
    "status_code": "400.111",
    "status_message": "Dynamic DNS account not enabled.",
}
```

The **status_message** provides a helpful one sentence description of the response.  

The additional **status_code** is typically used by specific apps that need more than the limited set of available HTTP response codes.  For example, the Dynamic DNS client uses several status codes to be compatible with other 3rd party Dynamic DNS clients.  For the most part, you can use the standard HTTP response codes for error handling, but check the documentation on API endpoints if you want to handle extended errors in a different way.

## Validation Handling
When a validation error occurs (HTTP Response code 422), the **validation_details** object is returned in the JSON payload.  Here's an example:

```json
{
    "status_code": "422.000",
    "status_message": "Validation error.",
    "validation_details": {
        "id": "System ID is invalid.",
        "name": "System name contains invalid characters."
    }
}
```