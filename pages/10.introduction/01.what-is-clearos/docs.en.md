---
title: What is ClearOS? A Developer View.
visible: true
taxonomy:
    category: docs
---

ClearOS is an open platform for deploying Server, Network, and Gateway functionality.

Think of ClearOS, newly installed on any hardware, as an Android phone, not for your but for your home or business. ClearOS has basic functionalities just as a smartphone does but much more. To expand upon that functionality, an admin/owner would use the ClearOS Marketplace (similar to the Google Play Store) to install individual applications (apps) and services that they can use to manage their home, business, or enterprise.  All of this is provided through an easy to use web-based administration tool, open APIs, and open standards.

ClearOS is based on the [CentOS Project](https://www.centos.org) and the [Open Source community at large](https://opensource.org/).  Users and developers of ClearOS benefit tremendously from the dedicated teams upstream that provide development, bug reporting, feature enhancements, packaging, translations and other resources to provide a stable and secure Operating System to act as the foundation to ClearOS.
## Adding Value
ClearOS adds value by:
1. Intelligently integrating open source projects, products, and services
2. Developing new and innovative custom apps and services
3. Providing a common framework for administrative management
4. Simplifying the installation process
5. Adding a Marketplace for developers of both free and paid software to showcase their apps and services

## Types of Apps
Most, if not all, of ClearOS apps can be placed in one of three categories:
1. Administrative or "Wrapper" app
2. Full stack app
3. Custom Development

### Administrative or "Wrapper" app
The majority of apps currently in the Marketplace consists of wrappers around established FOSS projects. These are often renamed to give more descriptive meaning to non-Linux users.  Examples include:
* SMTP Server (Postfix)
* IMAP and POP Server (Cyrus)
* Incoming Firewall (iptables)
* Software RAID Manager (mdadm)
* Intrusion Detection (Snort)

Along with simplifying the integration process and integrating the configuration with other apps, the administrative wrapper app exposes some (but certainly not all) configuration for the administrator to tweak the behavior. ClearOS typically does not aim to reveal every function or feature of an underlying application but rather focuses on tight integration and ease of use. That being said, ClearOS does not restrict skilled engineers from tinkering behind the scenes.

### Full Stack
A number of apps from FOSS projects and software vendors (ISVs) come with their own user interfaces.  In this case, the ClearOS app in the Marketplace will facilitate the installation and possibly provide some limited administration. Essentially, users and admins will jump from ClearOS's web-based administration tool to the dedicated full stack application's interface in order to further manage the app.  Examples of this type of app include:
* Plex Media Server (Plex)
* Kopano Messaging and Collaboration (Kopano)
* ownCloud Home and Business (ownCloud)

### Custom
The final class of apps consist of software written specifically for ClearOS. These apps may coordinate a broad combination of underlying apps or infrastructure in order to have outcomes but for the most part are items intrinsic to ClearOS only.  Examples include:
* Mail Archive
* Events Framework
* Users and Groups
* MultiWAN
* Marketplace
