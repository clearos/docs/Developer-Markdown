---
title: Introduction
published: true
taxonomy:
    category: docs
---

Welcome to the ClearOS Documentation.

ClearOS is an Open-Source, Linux-based OS platform that is designed from the ground up to be extensible. ClearOS provides any combination of network, gateway and server functionality to small/medium sized business, non-profit organizations, education/government and distributed enterprise.

Supporting best-in-class open source and 3rd party applications through one secure, intuitive web-based interface, ClearOS is about choice - giving administrators the ability to run services on-premise or in the cloud, or both - "Hybrid Technology".
