---
title: Webconfig
visible: true
taxonomy:
    category: docs
---

Webconfig is ClearOS's unified web-based GUI for administering the server.

![Webconfig Interface](./Webconfig.png?resize=400 "Webconfig Interface")

Webconfig has five main components:
1. Apache Engine
2. PHP Framework
3. Core (and Optional) apps
4. Themes
5. Actions and Events

## Apache Engine
Webconfig uses a sandboxed Apache service operating on custom port 81.  You can access Webconfig at:

    https://<IP_ADDR>:81
The sandboxed base path of the Apache installation is found here:

    /usr/clearos/sandbox

! The advantage of a sandboxed service is that Webonfig does not interfere with the normal operation of the default Apache (Web Server) service.  This standard instance is used in many apps such as running a typical web site, ownCloud, Kopano and others.

By default, your system will not have a proper certificate. ClearOS will make a self signed one that will always complain but is encrypted. You will get a certificate warning if any of the following apply:

* You are using an IP address instead of a hostname or the hostname doesn't match
* The date on your computer is outside the time range on the certificate
* Your browser doesn't trust the Certificate Authority. This is why the default certificate fails. You can get a valid certificate from a certificate provider or use the Let's Encrypt app.

![Webconfig Certificate Error](./CertificateError.png?resize=400 "Webconfig Certificate Error")

## PHP Framework
Webconfig uses the [CodeIgniter](https://codeigniter.com/) PHP framework.  The use of this framework brings the MVC model to development and abstracts away from the developer tasks like protection against CSRF and XSS attacks.

The CodeIgniter framework resides in:

    /usr/clearos/framework
    
## Core (and Optional) Apps
ClearOS core apps are those that are installed when the operating system is installed.  They are required for basic processes like starting/stopping daemons, reading and writing to the disk, basic networking etc.  Optional apps are installed via the Marketplace or from the command line, both using the the yum package manager to handle the install.

Both core and optional apps are installed to:

    /usr/clearos/apps

## Themes
Webconfig comes with a default theme.  Themes control the 'look and feel' of Webconfig and include the HTML markup, CSS and Javascript libraries and defined widgets which are used in almost all apps.  Themes are located in:

    /usr/clearos/themes

## Action and Events
ClearOS has an innovative and flexible action and events framework called clearsync.  This service can be used to trigger events and actions and is extensible to perform almost any task.

Configuration files (configlets) of the events system are located here:

    /etc/clearsync.d