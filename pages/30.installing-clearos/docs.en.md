---
title: Installing ClearOS
visible: true
taxonomy:
    category: docs
---

ClearOS is easy to install and get running. It also has great tools that can help you automate the installation. Whether you are installing one ClearOS System or thousands, we have great tools and training available so that you can become as much or as little of an expert as you want to be.

* [Preparation](preparation)
 * Steps to review before you start copying ClearOS to your server.
* [Installer](installer)
 * Using the Installer tools and software that will set up ClearOS for you.
* [First Boot](first-boot)
 * What to expect after you are installed and where to go from there.
* [Marketplace](marketplace)
 * Installing the applications and services that you need.
* [Upgrading](upgrading)
 * What to do if you are coming from an older version of ClearOS.
* [Appendix](installation-appendix)
 * Some things for advanced users and extra stuff.
