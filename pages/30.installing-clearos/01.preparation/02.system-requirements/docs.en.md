---
title: System Requirements
visible: true
taxonomy:
    category: docs
---

## System Requirements
You will need hardware of some type to run ClearOS. ClearOS works best as a server operating system on dedicated hardware or as a virtual server. It does not work well as a desktop operating system for users to use productivity apps or browse the web. You can use most any x86-based computer or server on the market today to run ClearOS. ClearOS 7 only supports 64-bit processors so make sure that your server is new enough to support this hardware architecture. If you are using ClearOS as a gateway, you will need at least two network interface cards, one for the internet side and one for the local network (LAN) side.

### HPE
Many models of HPE servers have been certified for running ClearOS. These models include:

| Family 1 | Family 2 | Family 3 | Family 4 | Family 5 |
| ----------- | ----------- | ----------- | ----------- |
| DL380 Gen9 | DL120 Gen9 | DL180 Gen9 | DL80 Gen9 | ML30 Gen9 |
| DL360 Gen9 | ML150 Gen9 | DL160 Gen9 | DL60 Gen9 | DL20 Gen9 |
| ML350 Gen9 | ML110 Gen9 |  |  |  |

### ClearBOX
You also have the option of purchasing ClearBOX hardware that is pre-tested and certified for use for ClearOS from ClearCenter.

## Summary
For hardware, virtual, and cloud environments, the base requirements are as follows:

### Base System
Processor/CPU	64-bit
Memory/RAM	At least 1 GB is recommended (see guidelines below)
Hard Disk	At least 10 GB is recommended (see guidelines below)
Network	Ethernet, Cable, DSL
For hardware installs, the requirements are as follows:

### Peripherals
Network Cards	A network card is required for install
DVD Drive	Required for DVD installation only
USB	Required for USB key installation only
Mouse	Not required
Monitor and Keyboard	Required for installation only

## Size Requirements
The following are guidelines for estimating the right hardware for your system. Keep in mind, the hardware required depends on how you use the software.

RAM and CPU	5 users	5-25 users	25-50 users	50-250 users	250+ users
Processor/CPU	Low-Power	Basic	Dual-Core	Quad-Core	Multi-Core + Multi-Processor
Memory/RAM	1-2 GB	2-4 GB	4-8 GB	8-16 GB	16-32 GB
Hard Disk
Hard Disk	Installation and logs require 2 GB. We recommend 20 GB or more - additional optional storage is up to you
RAID	Recommended for mission critical systems. Software RAID is supported and most hardware RAID cards are supported
The following software modules are processor and memory intensive:

Intrusion Detection and Prevention
Content Filtering
Zarafa See Hardware Recommendations
Kopano
Mail
Antispam
Antivirus
Hardware Requirements
CPU
All modestly modern hardware includes a compatible CPU. For those installing ClearOS on very old hardware, the CPU requirement is as follows:

Intel architectures from Pentium 6 and onward
AMD architectures from Athlon and onward
Network Cards
Generally, ClearOS does a good job at auto-detecting hardware and most mass-market network cards are supported. ClearBOX also includes wireless support. While many wireless cards will work with ClearOS, only a few are officially supported. VLANs are supported on ClearOS. If you use VLANs, you will have to have a VLAN capable switch that is also configured for tagging.

## Internet Connection
ClearOS supports most fiber, DSL (including PPPoE), and cable modem broadband Internet connections as well as standard Ethernet connections. ISDN and satellite broadband are not supported unless terminated with a standard Ethernet connection.

## RAID Support
See RAID Support.

## Links
Check out some additional resources to help you get your hardware configured and get the software ready for your installation.

Compatibility
Downloading
RAID Support
