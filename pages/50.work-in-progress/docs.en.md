---
title: Updating CentOS 7 to ClearOS 7
published: true
taxonomy:
    category: docs
---
# Prerequisites
You will need to start with a sparse install of CentOS available. For virtualization providers, find a very minimal instance type to use.

```
sudo su -
yum -y remove NetworkManager
# Set SELinux to disabled
/etc/selinux/config
reboot

```
## ClearOS Home and Business
It can be difficult to get business installed on a CentOS system that comes via a virtual platform like AWS because many of them run updates which will give you a very advanced version and depending on the update cycle, you may not be able to update unless you enable repos. When stuck, find the equivalent step in ClearOS community and run yum with --enablerepo=clearos-updates,clearos-centos

## ClearOS Community
```
sudo su -
mkdir -p /tmp/rpms
cd /tmp/rpms/
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/system-base-7.4.2-1.v7.x86_64.rpm
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/clearos-release-7-5.5.v7.x86_64.rpm
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/app-base-core-2.5.2-1.v7.noarch.rpm
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/app-base-2.5.2-1.v7.noarch.rpm
rpm -ivh --nodeps app-base-*
yum --enablerepo=* clean all
rpm -ivh --nodeps system-base-7.4.2-1.v7.x86_64.rpm
yum install clearos-release-7-5.5.v7.x86_64.rpm
yum reinstall app-base-*
yum --enablerepo=* clean all
yum reinstall system-base
yum reinstall clearos-release
yum --enablerepo=* clean all
systemctl stop webconfig
```

#### Edit the repos and enable items
In clearos-centos.repo and clearos.repo modify the enabled values for the following items:

```
vi /etc/yum.repos.d/clearos-centos.repo /etc/yum.repos.d/clearos.repo
```
#### Set a root password so that you can login to Webconfig
```
passwd
```

#### Run updates to basics and fixup nameserver then finish updates
```
yum --enablerepo=* clean all && yum -y update app-base
yum --enablerepo=* clean all && yum -y update
yum --enablerepo=* clean all
echo "nameserver 8.8.8.8" > /etc/resolv.conf
yum -y install app-accounts app-configuration-backup app-dashboard app-date app-dns app-edition app-events app-incoming-firewall app-groups app-language app-log-viewer app-mail app-marketplace app-process-viewer app-software-updates app-ssh-server app-support app-user-profile app-users
systemctl restart syswatch
allow-port -p TCP -d 22 -n SSH
allow-port -p TCP -d 81 -n Webconfig
systemctl start webconfig
systemctl enable webconfig
reboot
```





## Original StackScript from Linode
```
#!/bin/bash -x

(
ARCH=`arch`

# Prep release and repos
rpm -Uvh http://download2.clearsdn.com/marketplace/cloud/7/noarch/clearos-release-7-current.noarch.rpm
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-ClearOS-7

# Install and upgrade
yum --enablerepo=* clean all
yum --enablerepo=clearos-centos -y install app-base
yum --enablerepo=* clean all
service webconfig stop
yum --enablerepo=clearos-centos -y install app-accounts app-configuration-backup app-dashboard app-date app-dns app-edition app-events app-incoming-firewall app-groups app-language app-log-viewer app-mail app-marketplace app-process-viewer app-software-updates app-ssh-server app-support app-user-profile app-users

# Default networking
yum -y remove NetworkManager
echo "DEVICE=eth0" > /etc/sysconfig/network-scripts/ifcfg-eth0
echo "TYPE=\"Ethernet\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "ONBOOT=\"yes\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "USERCTL=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "BOOTPROTO=\"dhcp\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "PEERDNS=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0

echo "nameserver 8.8.8.8" > /etc/resolv-peerdns.conf
echo "nameserver 8.8.4.4" >> /etc/resolv-peerdns.conf

sed -i -e 's/^GATEWAYDEV=.*/GATEWAYDEV="eth0"/' /etc/sysconfig/network
sed -i -e 's/^EXTIF=.*/EXTIF="eth0"/' /etc/clearos/network.conf

service syswatch restart

# Enable firewall
allow-port -p TCP -d 22 -n SSH
allow-port -p TCP -d 81 -n Webconfig
sed -i -e 's/^MODE=.*/MODE="standalone"/' /etc/clearos/network.conf

# Start webconfig
service webconfig start

) 2>&1 | tee /var/log/clearos-installer.log

# Reboot
reboot
```
